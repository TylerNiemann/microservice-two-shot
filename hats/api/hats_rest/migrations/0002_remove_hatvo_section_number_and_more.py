# Generated by Django 4.0.3 on 2023-07-20 17:53

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='hatvo',
            name='section_number',
        ),
        migrations.RemoveField(
            model_name='hatvo',
            name='shelf_number',
        ),
    ]
