from django.http import JsonResponse
import json
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import Hat, HatVO

# Create your views here.
# get the encoders up and running and grab the data specifically want
# when it is called


class HatVODetailEncoder(ModelEncoder):
    model = HatVO
    properties = [
        "closet_name",
        # "section_number",
        # "shelf_number",
        "import_href"
        ]


class HatsListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "style_name",
        "color",
        "fabric",
        "id",
        ]

    def get_extra_data(self, o):

        return {"location": o.location.closet_name}


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "hat_url",
        "location",
    ]
    encoders = {
        "location": HatVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def hats_list(request):
    # hat_vo_id comes from url,
    if request.method == "GET":
        hats = Hat.objects.all()

        return JsonResponse(
            {"hats": hats},
            encoder=HatsListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            location_href = content["location"]
            location = HatVO.objects.get(import_href=location_href)
            content["location"] = location
        except HatVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location href"},
                status=400,
            )
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
@require_http_methods(["DELETE", "GET"])
def api_show_hat(request, pk):
    """
    Single-object API for the Location resource.

    GET:
    Returns the information for a Location resource based
    on the value of pk
    {
        "id": database id for the location,
        "closet_name": location's closet name,
        "section_number": the number of the wardrobe section,
        "shelf_number": the number of the shelf,
        "href": URL to the location,
    }

    PUT:
    Updates the information for a Location resource based
    on the value of the pk
    {
        "closet_name": location's closet name,
        "section_number": the number of the wardrobe section,
        "shelf_number": the number of the shelf,
    }

    DELETE:
    Removes the location resource from the application
    """
    if request.method == "GET":
        try:
            hat = Hat.objects.get(id=pk)
            return JsonResponse(
                hat,
                encoder=HatDetailEncoder,
                safe=False
            )
        except Hat.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            hat = Hat.objects.get(id=pk)
            hat.delete()
            return JsonResponse(
                hat,
                encoder=HatDetailEncoder,
                safe=False,
            )
        except Hat.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
