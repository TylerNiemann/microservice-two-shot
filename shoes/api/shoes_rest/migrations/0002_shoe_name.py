# Generated by Django 4.0.3 on 2023-07-19 23:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_rest', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='shoe',
            name='name',
            field=models.CharField(max_length=200, null=True),
        ),
    ]
