import React, { useState, useEffect } from 'react';


function HatForm() {
const [locations, setLocations] = useState([]);
const [fabric, setFabric] = useState('');
const [styleName, setStyleName] = useState('');
const [color, setColor] = useState('');
const [location, setLocation] = useState('');


  async function fetchLocations() {
    const url = 'http://localhost:8100/api/locations/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  }

  useEffect(() => {
    fetchLocations();
  }, [])







const handleHatSubmit = async (e) => {
    e.preventDefault();





    const data = {}

    data.fabric = fabric;
    data.style_name = styleName;
    data.color = color;
    data.location = location;



    const addHatUrl = 'http://localhost:8090/api/hats/';
    const fetchConfig = {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        }
    }
    const response = await fetch(addHatUrl, fetchConfig)
    if (response.ok) {
        // const newHat = await response.json()

        setFabric('');
        setStyleName('');
        setColor('');
        setLocation('');
    }
}


const handleFabricChange = (e) => {
    setFabric(e.target.value)
}
const handleStyleNameChange = (e) => {
    setStyleName(e.target.value)
}
const handleColorChange = (e) => {
    setColor(e.target.value)
}
const handleLocationChange = (e) => {
    setLocation(e.target.value)
}

return (
    <div>
        <div>
            <div>
                <h1>Create a hat!</h1>
                <form onSubmit={handleHatSubmit} id="create-hat-form">

                    <div>
                        <input value={ fabric } onChange={ handleFabricChange } placeholder="Fabric" required type="text" name="fabric" id="fabric" />
                        <label htmlFor="fabric">Fabric</label>
                    </div>
                    <div>
                        <input value={ styleName } onChange={ handleStyleNameChange } placeholder="Style Name" required type="text" name="style_name" id="style_name" />
                        <label htmlFor="style_name">Style Name</label>
                    </div>
                    <div>
                        <input value={ color } onChange={ handleColorChange } placeholder="Color" required type="text" name="color" id="color" />
                        <label htmlFor="color">Color</label>
                    </div>
                    <div>
                        <select onChange={ handleLocationChange } required id="location" name="location">
                            <option value="">Choose a location</option>
                            {locations.map(location => {
                            return (
                            <option key={ location.href } value={location.href}>{location.closet_name}
                        </option>
                         )
                        })}
                        </select>
                                    </div>
                                    <button className="btn btn-primary">Create</button>
                                </form>
                            </div>
                        </div>
                    </div>
                        );
                        }

    export default HatForm;
