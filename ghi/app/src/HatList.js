import React,{useEffect, useState} from "react";


function HatList() {
    const [hats, setHats] = useState([]);
    const fetchData = async () => {
        const url = 'http://localhost:8090/api/hats/'
        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json();
            setHats(data.hats)
        }
    }

async function DeleteHat(id) {
    const deleteHatUrl = `http://localhost:8090/api/hats/${id}/`;
    const fetchConfig = {
        method: 'DELETE',
        }
        const response = await fetch(deleteHatUrl, fetchConfig)
        if (response.ok){
            setHats(hats.filter(hat => hat.id!==id));
        }
    }


useEffect(() => {
        fetchData();
    }, []);

    return(
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Style Name</th>
                    <th>Color</th>
                    <th>Fabric</th>
                    <th>Id</th>
                    <th>Location</th>

                </tr>
            </thead>
            <tbody>
                {hats.map(hat => {
                    return (
                        <tr key={hat.href}>
                            <td>{ hat.style_name }</td>
                            <td>{ hat.color}</td>
                            <td>{ hat.fabric}</td>
                            <td>{ hat.id }</td>
                            <td>{ hat.location}</td>
                        <td>
                            <button onClick={() => DeleteHat(hat.id)}>Delete</button>
                        </td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    );
}

export default HatList;
