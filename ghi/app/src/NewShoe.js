import React, {useEffect, useState} from "react";

function NewShoe(){
    const [ bins, setBins ] = useState([]);
    const[ bin, setBin ] = useState('');
    const [shoe, setShoe] = useState('');
    const [color, setColor] = useState('');
    const [picture_url, setPicture_URL] = useState('');
    const [manufacturer, setManufacturer] = useState('');

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
          name: shoe,
          manufacturer: manufacturer,
          color: color,
          location: bin,
          picture_url: picture_url
        };
    
        const locationUrl = `http://localhost:8080/api/shoes/`;
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
          const newShoe = await response.json();
          console.log(newShoe);
          setBin('');
          setShoe('');
          setColor('');
          setManufacturer('');
          setPicture_URL('');
        }
      }




    async function getBins() {
        const response = await fetch('http://localhost:8100/api/bins/');
        if (response.ok) {
          const { bins } = await response.json();
          setBins(bins);
        } else {
          console.error('An error occurred fetching the data')
        }
      }

    useEffect(() => {
        getBins();
      }, [])

      function handleBinChange(event) {
        const { value } = event.target;
        setBin(value);
      }

      function handleShoeChange(event) {
        const { value } = event.target;
        setShoe(value);
      }

      
      function handleColorChange(event) {
        const { value } = event.target;
        setColor(value);
      }

      function handleManufacturerChange(event) {
        const { value } = event.target;
        setManufacturer(value);
      }

      function handlePictureURLChange(event) {
        const { value } = event.target;
        setPicture_URL(value);
      }

    return(
        <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new shoe</h1>
          <form onSubmit={handleSubmit} id="create-shoe-form">
            <div className="form-floating mb-3">
              <input  onChange={handleShoeChange} value={shoe} placeholder="Shoe name" required type="text" id="shoe_name" className="form-control" />
              <label htmlFor="shoe_name">Shoe name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleManufacturerChange} value={manufacturer}  placeholder="Manufacturer name" type="text" id="manufacturer_name" className="form-control" />
              <label htmlFor="manufacturer_name">Manufacturer name</label>
            </div>
            <div className="form-floating mb-3">
              <input  onChange={handleColorChange} value={color}  placeholder="Color" required type="text" id="color" className="form-control" />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input  onChange={handlePictureURLChange} value={picture_url}  placeholder="Picture_URL" required type="text" id="picture_url" className="form-control" />
              <label htmlFor="picture_url">Picture URL</label>
            </div>
            <div className="mb-3">
              <select onChange={handleBinChange}  value={bin} required className="form-select" id="bin">
                <option value="">Choose a bin</option>
                {bins.map(bin=> {
                  return (
                    <option key={bin.id} value={bin.href}>{bin.closet_name}</option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default NewShoe;