import React, {useEffect, useState} from "react";

function Shoe(){
    const [ shoes, setShoes ] = useState([]);

    async function getShoes() {
        const response = await fetch('http://localhost:8080/api/shoes/');
        if (response.ok) {
          const { shoes } = await response.json();
          setShoes(shoes);
        } else {
          console.error('An error occurred fetching the data')
        }
      }

    useEffect(() => {
        getShoes();
      }, [])

    async function handleDeleteShoe(id){
        const locationUrl = `http://localhost:8080/api/shoes/${id}/`;
        const fetchConfig = {
          method: "delete",
        };

        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            setShoes(shoes.filter(shoe => shoe.id !== id));
        }

      }

    return(
        <table className="table table-striped">
        <thead>
          <tr>
            <th>Shoe</th>
            <th>Manufacturer</th>
            <th>Color</th>
            <th>Bin</th>
          </tr>
        </thead>
        <tbody>
          {shoes.map(shoe => {
            return (
              <tr key={shoe.id}>
                <td>{ shoe.name }</td>
                <td>{ shoe.manufacturer }</td>
                <td>{ shoe.color }</td>
                <td>{ shoe.location }</td>
                <td>
                    <button onClick={() => handleDeleteShoe(shoe.id)}  className="btn btn-warning">Delete Shoe</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
}

export default Shoe;